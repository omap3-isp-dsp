CROSS_COMPILE ?=
KDIR ?=
MEDIACTL_DIR ?= /home/laurent/src/iob/arm

CC	:= $(CROSS_COMPILE)gcc
CFLAGS	:= -O2 -W -Wall -I$(KDIR)/usr/include -I$(MEDIACTL_DIR)/include
LDFLAGS	:= -Lbridge -L$(MEDIA_CTL_DIR)/lib
LIBS	:= -lmediactl -lv4l2subdev -ldspbridge -lrt

OBJECTS := isp-dsp.o v4l2.o

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

all: __bridge __dsp isp-dsp

isp-dsp: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

__bridge:
	$(MAKE) -C bridge CROSS_COMPILE=$(CROSS_COMPILE)

__dsp:
	$(MAKE) -C dsp

clean:
	$(MAKE) -C dsp clean
	$(MAKE) -C bridge clean
	-$(RM) *.o
	-$(RM) isp-dsp

