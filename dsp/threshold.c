/*
 * OMAP3 DSP sample image threshold algorithm
 *
 * Copyright (C) 2010-2011 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stddef.h>
#include "node.h"

unsigned int threshold_create(void)
{
	return 0x8000;
}

unsigned int threshold_delete(void)
{
	return 0x8000;
}

static void threshold_process(uint16_t *data, unsigned int size)
{
	unsigned int i;

	for (i = 0; i < size / 2; ++i)
		data[i] = data[i] >= 512 ? 1023 : 0;
}

unsigned int threshold_execute(void *env)
{
	unsigned char done = 0;
	unsigned int size;
	dsp_msg_t msg;
	void *buffer;

	while (!done) {
		NODE_getMsg(env, &msg, (unsigned) -1);

		switch (msg.cmd) {
		case 0:
			buffer = (void *)msg.arg_1;
			size = (unsigned int)msg.arg_2;

			BCACHE_inv(buffer, size, 1);
			threshold_process(buffer, size);
			BCACHE_wb(buffer, size, 1);

			NODE_putMsg(env, NULL, &msg, 0);
			break;

		case 0x80000000:
			done = 1;
			break;
		}
	}

	return 0x8000;
}
